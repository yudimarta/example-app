import login from '../../api/authApi';

export default function fetchLogin(username, password) {
  return async (dispatch) => {
    dispatch({
      type: 'FETCH_LOGIN_REQUEST'
    });

    try {
      const result = await login(username, password);

      if (result.status === 200) {
        dispatch({
          type: 'FETCH_LOGIN_SUCCESS',
          payload: result.data
        });
      } else {
        dispatch({
          type: 'FETCH_LOGIN_FAILED',
          error: result.data
        });
      }
    } catch (err) {
      dispatch({
        type: 'FETCH_LOGIN_FAILED',
        error: err
      });
    }
  };
}

