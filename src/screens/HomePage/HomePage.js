import React from 'react';
import { View, Text, FlatList } from 'react-native';
import { connect } from 'react-redux';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
];

function noData() {
  return (
    <View>
      <Text>no data</Text>
    </View>
  )
}

class HomePage extends React.Component {
  constructor() {
    super();
    this.state = {};
  }

  // componentDidMount() {
    
  // }
  
  // async fetchLogin(val) {
  //   const { navigation } = this.props;
  //   try {
  //     let response = await fetch('https://api-nodejs-todolist.herokuapp.com/user/login', {
  //       method: 'POST',
  //       headers: {
  //         Accept: 'application/json',
  //         'Content-Type': 'application/json',
  //       },
  //       body: JSON.stringify(val),
  //     });
  //     let result = await response.json();

  //     if (response.status === 200) {
  //       AsyncStorage.setItem('@token', result.token);
  //       navigation.navigate('TabNavigator');
  //     } else {
  //       alert('error')
  //     }
  //   } catch (error) {
  //     console.error(error);
  //   }
  // }

  renderItem({ item }) {
    console.log({item});
    return (
      <View style={{ backgroundColor: 'red' }}>
        <Text>{item.title}</Text>
      </View>
    );
  }

  render() {
    return (
      <View>
        <Text>Home Page</Text>

        {/* <FlatList
            data={DATA}
            renderItem={this.renderItem}
            keyExtractor={item => item.id}
          /> */}

        {/* {
          DATA.map((item, i) => (
            <View key={i} style={{ backgroundColor: 'red' }}>
              <Text>{item.title}</Text>
            </View>
          ))
        }

        {
          DATA.map((item, i) => {
            return (
              <View key={i} style={{ backgroundColor: 'blue' }}>
                <Text>{item.title}</Text>
              </View>
            );
          })
        } */}

        {/* {DATA.length > 0 ? (
          DATA.map((item, i) => (
            <View key={i} style={{ backgroundColor: 'red' }}>
              <Text>{item.title}</Text>
            </View>
          ))
        ) : (
          noData()
        )} */}
        <View style={{ flex: 1,flexDirection: 'row' }}>
          <View style={{flex: 1}}>
            <Text style={{ textAlign: 'center'}}>Name: {this.props.user.name}</Text>
          </View>
          <View style={{flex: 1}}>
            <Text  style={{ textAlign: 'center'}}>Age: {this.props.user.age}</Text>
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state.authStore.payload;
  return {
    user
  }
}

export default connect(mapStateToProps, null)(HomePage);
