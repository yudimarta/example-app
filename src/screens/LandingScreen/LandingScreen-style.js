import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageBackground: {
    flex: 1,
    resizeMode: 'cover',
  },
  title: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 18,
    paddingTop: 10,
  },
  subtitle: {
    color: 'white',
    fontSize: 14,
    textAlign: 'center',
    paddingTop: 20,
  },
  footer: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: 20,
    alignItems: 'center',
  },
  img: {
    width: 100,
    height: 100,
    borderRadius: 50,
  },
});

export default styles;
