import React, { useState, useEffect } from 'react';
import {
  ImageBackground,
  Image,
  Text,
  TextInput,
  View
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';


import fetchAxios from '../../utils/fetchAxios';
import { PrimaryButton } from '../../components';

import fetchLogin from '../../stores/actions/authActions';

import styles from './LoginScreen-style';

import imgBackground from '../../assets/backgroundimg.jpeg';
const image = {uri: 'https://reactjs.org/logo-og.png'};

function LoginScreen(props) {
  const [email, setEmail] = useState('irwin_prt@email.com');
  const [password, setPassword] = useState('12345678');

  const handleLogin = async (val) => {
    try {
      // const response = await axios.post('https://api-nodejs-todolist.herokuapp.com/user/login', val);
      // const response = await fetchAxios('POST', 'user/login', val);
      // console.log('res: ==222 ', response);
      console.log('==handleLogin ');

      props.dispatchLogin(email, password);

      // AsyncStorage.setItem('@token', response.token);
      // navigation.navigate('TabNavigator');
    } catch (e) {
      alert (e)
      console.error(e);
    }
  };

  // console.log(email);

  useEffect(() => {
    const { navigation } = props;
    if (props.authStore.payload.token && props.authStore.payload.token !== '') {
      AsyncStorage.setItem('@token', props.authStore.token);
      navigation.navigate('TabNavigator');
    }

  }, [props.authStore.payload])

  console.log('authStore: ', props.authStore)

  return (
    <View style={styles.container}>
      <ImageBackground source={imgBackground} style={styles.imageBackground}>
        <View style={{alignItems: 'center', paddingTop: 40 }}>
          <Image source={image} style={styles.img} />

          <Text style={styles.title}>
            Saatnya bererkeasi yang bernilai edukasi
          </Text>

          <View style={{ paddingTop: 20 }}>
            <TextInput
              style={{ height: 40, width: 300, color: '#FFF' }}
              underlineColorAndroid="#FFF"
              borderBottomWidth={0}
              placeholderTextColor="#FFF"
              placeholder="Email"
              onChangeText={(val) => setEmail(val)}
              value={email}
            />
          </View>

          <View style={{ paddingTop: 20 }}>
            <TextInput
              style={{ height: 40, width: 300, color: '#FFF' }}
              underlineColorAndroid="#FFF"
              borderBottomWidth={0}
              placeholderTextColor="#FFF"
              placeholder="Kata Sandi"
              secureTextEntry
              onChangeText={(val) => setPassword(val)}
              value={password}
            />
          </View>
        </View>

        <View style={{alignItems: 'center', paddingTop: 40 }}>
          <View style={{paddingBottom: 10, width: 300, borderRadius: 10}}>
            <PrimaryButton title="Masuk" onPress={() => handleLogin({email, password})} />
          </View>
          <View>
            <Text>Belum punya akun</Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  )
}

function mapStateToProps(state) {
  return {
    authStore: state.authStore,
    isLoading: state.authStore.isLoading
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchLogin: (email, password) => dispatch(fetchLogin(email, password)),
    // increment: () => dispatch({
    //   type: 'INCREMENT'
    // })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
